package com.scribd.challenge.weatherforcast;

import androidx.lifecycle.MutableLiveData;

import com.scribd.challenge.weatherforcast.models.DailyWeather;
import com.scribd.challenge.weatherforcast.models.WeatherInformationResponse;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class WeatherService {

    private WeatherAPI weatherAPI;
    private final MutableLiveData<List<DailyWeather>> mutableLiveData;

    public WeatherService(){
        weatherAPI = NetworkManager.createRetrofitService();
        mutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<List<DailyWeather>> getMutableLiveData() {
        return mutableLiveData;
    }

    public void getWeatherForecastForCity(String city){
        weatherAPI.getForecast(city, Secret.APP_ID).enqueue(new Callback<WeatherInformationResponse>() {
            @Override
            public void onResponse(Call<WeatherInformationResponse> call, Response<WeatherInformationResponse> response) {
                if(response.isSuccessful()){
                    List<DailyWeather> dailyWeatherList = Arrays.asList(response.body().getWeatherArray());
                    mutableLiveData.postValue(dailyWeatherList);
                }

            }

            @Override
            public void onFailure(Call<WeatherInformationResponse> call, Throwable t) {
                    mutableLiveData.postValue(null);
            }
        });
    }
    public interface WeatherAPI {

        @GET("data/2.5/forecast")
        Call<WeatherInformationResponse> getForecast(@Query("q")String city, @Query("appid")String appid);
    }
}
