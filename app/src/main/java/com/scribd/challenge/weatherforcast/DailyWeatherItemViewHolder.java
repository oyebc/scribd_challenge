package com.scribd.challenge.weatherforcast;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scribd.challenge.weatherforcast.databinding.WeatherItemBinding;
import com.scribd.challenge.weatherforcast.models.DailyWeather;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DailyWeatherItemViewHolder extends RecyclerView.ViewHolder {

    private final TextView dateTextView, descriptionTextView, temperatureTextView;

    public DailyWeatherItemViewHolder(@NonNull WeatherItemBinding binding) {
        super(binding.getRoot());
        dateTextView = binding.dateTextView;
        descriptionTextView = binding.descriptionTextView;
        temperatureTextView = binding.temperatureTextView;
    }

    public void bindToModel(DailyWeather dailyWeather){

        Date date = new Date();
        date.setTime(dailyWeather.getTimestamp());
        String formattedDate = new SimpleDateFormat("dd")
                .format(date);
        dateTextView.setText(formattedDate);
        descriptionTextView.setText(dailyWeather.getWeatherDetails()[0].getDescription());
        temperatureTextView.setText(dailyWeather.getTemperaturePoints().getTemp() + " F");
    }
}
