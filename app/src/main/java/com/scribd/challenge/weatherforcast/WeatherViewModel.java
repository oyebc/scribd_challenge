package com.scribd.challenge.weatherforcast;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.scribd.challenge.weatherforcast.models.DailyWeather;

import java.util.List;

public class WeatherViewModel extends AndroidViewModel {

    private LiveData<List<DailyWeather>> weatherLiveData;
    private WeatherService weatherService;

    public WeatherViewModel(@NonNull Application application) {
        super(application);
        weatherService = new WeatherService();
    }

    public LiveData<List<DailyWeather>> getWeatherLiveData(){
        return weatherService.getMutableLiveData();
    }

    public void checkForecastForCity(String city){
        weatherService.getWeatherForecastForCity(city);
    }

}
