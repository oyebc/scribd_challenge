package com.scribd.challenge.weatherforcast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.scribd.challenge.weatherforcast.databinding.WeatherItemBinding;
import com.scribd.challenge.weatherforcast.models.DailyWeather;

import java.util.ArrayList;
import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<DailyWeatherItemViewHolder> {

    private List<DailyWeather> dailyWeatherList = new ArrayList<>();

    @NonNull
    @Override
    public DailyWeatherItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        WeatherItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_daily_weather_view, parent, false);

        return new DailyWeatherItemViewHolder(binding);
    }

    public void refreshList(List<DailyWeather> dailyWeatherList){
        this.dailyWeatherList = dailyWeatherList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull DailyWeatherItemViewHolder holder, int position) {
        holder.bindToModel(dailyWeatherList.get(position));
    }

    @Override
    public int getItemCount() {
        return dailyWeatherList.size();
    }
}
