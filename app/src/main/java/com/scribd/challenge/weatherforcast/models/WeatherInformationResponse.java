package com.scribd.challenge.weatherforcast.models;

import com.google.gson.annotations.SerializedName;

public class WeatherInformationResponse {

    private String cod;
    private float message;
    private float cnt;
    @SerializedName("list")
    private DailyWeather[] weatherArray;

    public String getCod() {
        return cod;
    }

    public float getMessage() {
        return message;
    }

    public float getCnt() {
        return cnt;
    }

    public DailyWeather[] getWeatherArray() {
        return weatherArray;
    }
}
