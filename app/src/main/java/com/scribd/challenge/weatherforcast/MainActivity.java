package com.scribd.challenge.weatherforcast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.scribd.challenge.weatherforcast.databinding.MainActivityBinding;
import com.scribd.challenge.weatherforcast.models.DailyWeather;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MainActivityBinding binding;
    private WeatherViewModel viewModel;
    private WeatherAdapter weatherListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);

        viewModel.getWeatherLiveData().observe(this, new Observer<List<DailyWeather>>() {
            @Override
            public void onChanged(List<DailyWeather> dailyWeathers) {
                if(dailyWeathers ==null){
                    Toast.makeText(MainActivity.this, "Error occured while requesting forecast data", Toast.LENGTH_SHORT);
                    return;
                }

                if(weatherListAdapter == null) {
                    setupRecyclerView(dailyWeathers);
                }

                weatherListAdapter.refreshList(dailyWeathers);
                binding.progressBar.setVisibility(View.GONE);
            }
        });

        binding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.searchView.getQuery() == null) return;
                String query = binding.searchView.getQuery().toString();
                viewModel.checkForecastForCity(query);
                binding.progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setupRecyclerView(List<DailyWeather> dailyWeathers) {
        weatherListAdapter = new WeatherAdapter();
        RecyclerView weatherList = binding.recyclerView;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        weatherList.setLayoutManager(layoutManager);
        weatherList.setAdapter(weatherListAdapter);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());
        weatherList.addItemDecoration(itemDecoration);

    }


}
