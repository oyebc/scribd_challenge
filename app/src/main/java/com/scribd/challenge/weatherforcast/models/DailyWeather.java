package com.scribd.challenge.weatherforcast.models;

import com.google.gson.annotations.SerializedName;

public class DailyWeather {

    private long timestamp;
    @SerializedName("main")
    private TemperaturePoints temperaturePoints;
    @SerializedName("weather")
    private WeatherDetails[] weatherDetails;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public TemperaturePoints getTemperaturePoints() {
        return temperaturePoints;
    }

    public void setTemperaturePoints(TemperaturePoints temperaturePoints) {
        this.temperaturePoints = temperaturePoints;
    }

    public WeatherDetails[] getWeatherDetails() {
        return weatherDetails;
    }
}
