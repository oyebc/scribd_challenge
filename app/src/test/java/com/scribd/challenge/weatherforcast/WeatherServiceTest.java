package com.scribd.challenge.weatherforcast;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.*;


@RunWith(RobolectricTestRunner.class)
public class WeatherServiceTest {

    @Test
    public void getWeatherForecastForCity() {

        WeatherService weatherService = new WeatherService();
        LiveData<Object> liveData = weatherService.getMutableLiveData();

        liveData.observe(ApplicationProvider.getApplicationContext(), new Observer<Object>() {
            @Override
            public void onChanged(Object o) {
                assertNotNull(o);
            }
        });

    }
}